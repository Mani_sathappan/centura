#include <os.h>
#include <math.h>
#include <stdio.h>
extern OS_MEM SplayPartition;
extern CPU_INT08U SplayPartitionStorage[12][100];

splay* root=NULL;
OS_ERR  err;   
/*********************************Splay tree*******************************************/


splay* RR_Rotate(splay* k2)
{
	splay* k1 = k2->lchild;
	k2->lchild = k1->rchild;
	k1->rchild = k2;
	return k1;
}


splay* LL_Rotate(splay* k2)
{
	splay* k1 = k2->rchild;
	k2->rchild = k1->lchild;
	k1->lchild = k2;
	return k1;
}


splay* Splay(OS_TICK next_release, splay* root)
{
	if(!root)
                  return root;
	splay header;
	/* header.rchild points to L tree; header.lchild points to R Tree */
	header.lchild = header.rchild = NULL;
	splay* LeftTreeMax = &header;
	splay* RightTreeMin = &header;


	while(1)
	{
		if(next_release < root->p_tcb_splay->Next_Release)
		{
			if(!root->lchild)
				break;
			if(next_release < root->lchild->p_tcb_splay->Next_Release)
			{
				root = RR_Rotate(root); 
				if(!root->lchild)
					break;
			}
		
			RightTreeMin->lchild = root;
			RightTreeMin = RightTreeMin->lchild;
			root = root->lchild;
			RightTreeMin->lchild = NULL;
		}
		else if(next_release > root->p_tcb_splay->Next_Release)
		{
			if(!root->rchild)
				break;
			if(next_release > root->rchild->p_tcb_splay->Next_Release)
			{
				root = LL_Rotate(root);
				if(!root->rchild)
					break;
			}
			/* Link to L Tree */
			LeftTreeMax->rchild = root;
			LeftTreeMax = LeftTreeMax->rchild;
			root = root->rchild;
			LeftTreeMax->rchild = NULL;
		}
		else
			break;
	}

	LeftTreeMax->rchild = root->lchild;
	RightTreeMin->lchild = root->rchild;
	root->lchild = header.rchild;
	root->rchild = header.lchild;

	return root;
}

splay* Node(OS_TCB *p_tcb_)
{

        splay *p_node = (splay *)OSMemGet((OS_MEM *)&SplayPartition,(OS_ERR *)&err);
	
	p_node->p_tcb_splay = p_tcb_;
	p_node->lchild = p_node->rchild = NULL;
        return p_node;
}

splay* Insert(OS_TCB *p_tcb_, splay* root)
{
	splay* p_node = NULL;  
        p_node = (splay *)OSMemGet((OS_MEM *)&SplayPartition,(OS_ERR *)&err);
        
	p_node->p_tcb_splay = p_tcb_;
	p_node->lchild = p_node->rchild = NULL;
        
        /***************initialize the stack*************/
        p_node->p_tcb_splay->StkPtr = OSTaskStkInit(p_node->p_tcb_splay->TaskEntryAddr,
                                                    p_node->p_tcb_splay->TaskEntryArg,
                                                    p_node->p_tcb_splay->StkBasePtr,
                                                    p_node->p_tcb_splay->StkLimitPtr,
                                                    p_node->p_tcb_splay->StkSize,
                                                    p_node->p_tcb_splay->Opt);
        
        
        if(!root)
	{
		root = p_node;
		p_node = NULL;
		return root;
	}
	root = Splay(p_tcb_->Next_Release, root);
	if(p_tcb_->Next_Release == root->p_tcb_splay->Next_Release)   //duplicate values
    {
        if(root->duplicate_nbr == 0)
        {
            root->next = p_node;
            root->duplicate_nbr++;
            p_node = NULL;
            return root;
        }
        else
        {
            splay *temp = root;
            (temp->duplicate_nbr)++;
            while(temp->next != NULL)
            {
                temp = temp->next;
            }
            temp->next = p_node;
            p_node = NULL;
            return root;
        }
    }

	if(p_tcb_->Next_Release < root->p_tcb_splay->Next_Release)
	{
		p_node->lchild = root->lchild;
		p_node->rchild = root;
		root->lchild = NULL;
		root = p_node;

	}
	else if(p_tcb_->Next_Release > root->p_tcb_splay->Next_Release)
	{
		p_node->rchild = root->rchild;
		p_node->lchild = root;
		root->rchild = NULL;
		root = p_node;
 
	}
	else
		return root;
	p_node = NULL;
	return root;
}

/****************************************DELETE TASK**********************************************/
splay* Delete(OS_TCB *p_tcb_, splay* root)
{
	splay* temp;
        splay* prev_temp;
	if(!root)
                  return root;
	root = Splay(p_tcb_->Next_Release, root);
	if(p_tcb_->Next_Release != root->p_tcb_splay->Next_Release) 
		return root;
        else
	{
                
                if((root->p_tcb_splay == p_tcb_) &&(root->duplicate_nbr == 0))
                {
                  if(!root->lchild)
                  {
                          temp = root;
                          root = root->rchild;
                  }
                  else
                  {
                          temp = root;

                          root = Splay(p_tcb_->Next_Release, root->lchild);
                          root->rchild = temp->rchild;
                  }
                  OSMemPut((OS_MEM *)&SplayPartition,(splay *)temp,(OS_ERR *)&err);
                  //free(temp);
                  return root;
                 }
                
                else if((root->p_tcb_splay == p_tcb_) &&(root->duplicate_nbr > 0))
                {
                  temp = root->next;
                  root->duplicate_nbr = root->duplicate_nbr - 1;
                  root->next = temp->next;
                  root->p_tcb_splay = temp->p_tcb_splay;
                  OSMemPut((OS_MEM *)&SplayPartition,(splay *)temp,(OS_ERR *)&err);
                  return root;
                }
                

                else if((root->duplicate_nbr > 0) && (root->p_tcb_splay != p_tcb_))
                {
                  temp = root;
                  while(temp->p_tcb_splay != p_tcb_)
                  {
                    prev_temp = temp;
                    temp = temp->next;
                  }
                  root->duplicate_nbr = root->duplicate_nbr - 1;
                  prev_temp->next = temp->next;
                  OSMemPut((OS_MEM *)&SplayPartition,(splay *)temp,(OS_ERR *)&err);
                  return root;
                }
        }
}

splay* Search(OS_TICK key, splay* root)
{
	return Splay(key, root);
}

