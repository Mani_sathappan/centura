#include <os.h>
#include <stddef.h>
#include <stdio.h>
#include "splaytree.h"

OS_MEM MemBlock;

splayTree * splaytree (CPU_INT32U i, splayTree * t) {
/* Simple top down splay, not requiring i to be in the tree t.  */
/* What it does is described above.                             */
    splayTree N, *l, *r, *y;
    if (t == NULL) return t;
    N.left = N.right = NULL;
    l = r = &N;

    for (;;) {
	if (i < t->p_tcb->Period) {
	    if (t->left == NULL) break;
	    if (i < t->left->p_tcb->Period) {
		y = t->left;                           /* rotate right */
		t->left = y->right;
		y->right = t;
		t = y;
		if (t->left == NULL) break;
	    }
	    r->left = t;                               /* link right */
	    r = t;
	    t = t->left;
	} else if (i > t->p_tcb->Period) {
	    if (t->right == NULL) break;
	    if (i > t->right->p_tcb->Period) {
		y = t->right;                          /* rotate left */
		t->right = y->left;
		y->left = t;
		t = y;
		if (t->right == NULL) break;
	    }
	    l->right = t;                              /* link left */
	    l = t;
	    t = t->right;
	} else {
	    break;
	}
    }
    l->right = t->left;                                /* assemble */
    r->left = t->right;
    t->left = N.right;
    t->right = N.left;
    return t;
}

//Tree * insert(CPU_INT32U period, CPU_INT32U i, Tree * t, OS_TCB * p_tcb) {
splayTree * inserttree(splayTree * t, OS_TCB * p_tcb, OS_MUTEX *p_mutex){

/* Insert i into the tree t, unless it's already there.    */
/* Return a pointer to the resulting tree.                 */
    splayTree * new;
    
    OS_ERR err;
    
    new = (splayTree *)OSMemGet(&MemBlock,&err);
     
    if (new == NULL) {
	//exit(1);                   //new mem is always NULL in ucos so exits
    }
    
    new->p_tcb = p_tcb;
    new->p_mutex = p_mutex;

    if (t == NULL) {
	new->left = new->right = NULL;
	return new;
    }
    t = splaytree(p_tcb->Period,t);
    if (new->p_tcb->Period < t->p_tcb->Period){
	new->left = t->left;
	new->right = t;
	t->left = NULL;
	return new;
    } else if (new->p_tcb->Period > t->p_tcb->Period) {
	new->right = t->right;
	new->left = t;
	t->right = NULL;
	return new;
    }
    else { /* We get here if it's already in the tree */
             /* Don't add it again                      */
        OSMemPut(&MemBlock,(void *)new, &err); //change
	return t;
    }
}

splayTree * deletetree(splayTree * t, OS_TCB * p_tcb) {
/* Deletes i from the tree if it's there.               */
/* Return a pointer to the resulting tree.              */
    
    OS_ERR err;
    
    splayTree * x;
    
    if (t==NULL) return NULL;

    t = splaytree(p_tcb->Period,t);

    if (p_tcb->Period == t->p_tcb->Period) {               /* found it */
	if (t->left == NULL) {
	    x = t->right;
	} else {
	    x = splaytree(p_tcb->Period, t->left);
	    x->right = t->right;
	}
	OSMemPut(&MemBlock,(void *)t,&err); //change
	return x;
    }
    
    return t;                         /* It wasn't there */
}

// Returns minimum value in a given Binary tree

int minvalue(splayTree * node)
{

	splayTree* current = node;

	/* loop down to find the leftmost leaf */
        while(current->left != NULL){
          current = current->left;
        }
	return(current->p_tcb->Prio);
}

void preOrder(splayTree *root)
{
    if (root != NULL)
    {
        printf("%d \n", root->p_tcb->Period);
        preOrder(root->left);
        preOrder(root->right);
    }
}