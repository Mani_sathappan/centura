#include <stdio.h>
#include <os.h>

typedef struct node
{
    int data;
    struct node *left,*right;
    int ht;
    OS_MUTEX *p_mutex;
}node;

extern OS_MEM avlPartition;
extern CPU_INT08U avl_mem_alloc[12][100];

node *insert(node *,OS_MUTEX *, int, int);
node *delete(node *,OS_MUTEX *, int);
void preorder(node *);
void inorder(node *);
int height( node *);
node *rotateright(node *);
node *rotateleft(node *);
node *RR(node *);
node *LL(node *);
node *LR(node *);
node *RL(node *);
int BF(node *);