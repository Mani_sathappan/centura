#include <os.h>
#include <math.h>
#include <stdio.h>
extern OS_MEM HeapPartition;
extern CPU_INT08U HeapPartitionStorage[12][100];
OS_TCB *rdylistptr;

heap* Tree=NULL;
OS_ERR  heap_err;
/*********************************Heap*******************************************/


heap* RR_Rotate_heap(heap* k2)
{
  heap* k1 = k2->lchild;
  k2->lchild = k1->rchild;
  k1->rchild = k2;
  return k1;
}


heap* LL_Rotate_heap(heap* k2)
{
  heap* k1 = k2->rchild;
  k2->rchild = k1->lchild;
  k1->lchild = k2;
  return k1;
}


heap* Heap(OS_TICK period, heap* Tree)
{
  if(!Tree)
    return Tree;

  heap head;

  head.lchild = head.rchild = NULL;
  heap* LeftTreeMax_heap = &head;
  heap* RightTreeMin_heap = &head;
  

  while(1)
  {
    if(period < Tree->p_tcb_heap->Period)
    {
      if(!Tree->lchild)
        break;
      if(period < Tree->lchild->p_tcb_heap->Period)
      {
        Tree = RR_Rotate_heap(Tree); 
        if(!Tree->lchild)
          break;
      }
      /* Link to R Tree */
      RightTreeMin_heap->lchild = Tree;
      RightTreeMin_heap = RightTreeMin_heap->lchild;
      Tree = Tree->lchild;
      RightTreeMin_heap->lchild = NULL;
    }
    else if(period > Tree->p_tcb_heap->Period)
    {
      if(!Tree->rchild)
        break;
      if(period > Tree->rchild->p_tcb_heap->Period)
      {
        Tree = LL_Rotate_heap(Tree);
        if(!Tree->rchild)
          break;
      }
      /* Link to L Tree */
      LeftTreeMax_heap->rchild = Tree;
      LeftTreeMax_heap = LeftTreeMax_heap->rchild;
      Tree = Tree->rchild;
      LeftTreeMax_heap->rchild = NULL;
    }
    else
      break;
  }

  LeftTreeMax_heap->rchild = Tree->lchild;
  RightTreeMin_heap->lchild = Tree->rchild;
  Tree->lchild = head.rchild;
  Tree->rchild = head.lchild;
  
  return Tree;
}

heap* New_Node_heap(OS_TCB *p_tcb_)
{
  
  heap *p_node = (heap *)OSMemGet((OS_MEM *)&HeapPartition,(OS_ERR *)&heap_err);
 
  p_node->p_tcb_heap = p_tcb_;
  p_node->lchild = p_node->rchild = NULL;
  return p_node;
}


heap* Insert_heap(OS_TCB *p_tcb_, heap* Tree)
{
  
  heap* p_node = NULL;
  
  p_node = (heap *)OSMemGet((OS_MEM *)&HeapPartition,(OS_ERR *)&heap_err);

  p_node->p_tcb_heap = p_tcb_;
  p_node->lchild = p_node->rchild = NULL;
  
  /***************initialize the stack*************/
  p_node->p_tcb_heap->StkPtr = OSTaskStkInit(p_node->p_tcb_heap->TaskEntryAddr,
                                             p_node->p_tcb_heap->TaskEntryArg,
                                             p_node->p_tcb_heap->StkBasePtr,
                                             p_node->p_tcb_heap->StkLimitPtr,
                                             p_node->p_tcb_heap->StkSize,
                                             p_node->p_tcb_heap->Opt);
  
  
  if(!Tree)
  {

    Tree = p_node;
    p_node = NULL;
    return Tree;
  }
  Tree = Heap(p_tcb_->Period, Tree);
  if(p_tcb_->Period == Tree->p_tcb_heap->Period)   //duplicate values
  {
    if(Tree->dup_nbr == 0)
    {
      Tree->next = p_node;
      Tree->dup_nbr++;
      p_node = NULL;
      return Tree;
    }
    else
    {
      heap *temp = Tree;
      (temp->dup_nbr)++;
      while(temp->next != NULL)
      {
        temp = temp->next;
      }
      temp->next = p_node;
      p_node = NULL;
      return Tree;
    }
  }

  if(p_tcb_->Period < Tree->p_tcb_heap->Period)
  {
    p_node->lchild = Tree->lchild;
    p_node->rchild = Tree;
    Tree->lchild = NULL;
    Tree = p_node;

  }
  else if(p_tcb_->Period > Tree->p_tcb_heap->Period)
  {
    p_node->rchild = Tree->rchild;
    p_node->lchild = Tree;
    Tree->rchild = NULL;
    Tree = p_node;

  }
  else
    return Tree;
  p_node = NULL;
  return Tree;
}

/****************************************DELETE TASK**********************************************/
heap* Delete_heap(OS_TCB *p_tcb_, heap* Tree)
{
  heap* temp;
  heap* prev_temp;
  if(!Tree)
    return Tree;
  //return NULL;
  Tree = Heap(p_tcb_->Period, Tree);
  if(p_tcb_->Period != Tree->p_tcb_heap->Period) 
    return Tree;
  
  else
  {
    
    if((Tree->p_tcb_heap == p_tcb_) &&(Tree->dup_nbr == 0))
    {
      if(!Tree->lchild)
      {
        temp = Tree;
        Tree = Tree->rchild;
      }
      else
      {
        temp = Tree;

        Tree = Heap(p_tcb_->Period, Tree->lchild);
        Tree->rchild = temp->rchild;
      }
      OSMemPut((OS_MEM *)&HeapPartition,(heap *)temp,(OS_ERR *)&heap_err);
      //free(temp);
      return Tree;
    }

    else if((Tree->p_tcb_heap == p_tcb_) &&(Tree->dup_nbr > 0))
    {
      temp = Tree->next;
      Tree->dup_nbr = Tree->dup_nbr - 1;
      Tree->next = temp->next;
      Tree->p_tcb_heap = temp->p_tcb_heap;
      OSMemPut((OS_MEM *)&HeapPartition,(heap *)temp,(OS_ERR *)&heap_err);
      return Tree;
    }
    
    else if((Tree->dup_nbr > 0) && (Tree->p_tcb_heap != p_tcb_))
    {
      temp = Tree;
      while(temp->p_tcb_heap != p_tcb_)
      {
        prev_temp = temp;
        temp = temp->next;
      }
      Tree->dup_nbr = Tree->dup_nbr - 1;
      prev_temp->next = temp->next;
      OSMemPut((OS_MEM *)&HeapPartition,(heap *)temp,(OS_ERR *)&heap_err);
      return Tree;
    }
  }
}

void ReadyListInsert(void)
{
  heap *MinNode = find_min();
  
  if(MinNode == NULL)
  {
  }
  else if(rdylistptr == NULL)
  {
    if(MinNode != NULL)
    {
      OS_TaskRdy(MinNode->p_tcb_heap);
      rdylistptr = MinNode->p_tcb_heap;
      OSTaskQty++;
      Tree = Delete_heap(MinNode->p_tcb_heap, Tree);
    }
  }
  
  else if(MinNode->p_tcb_heap-> Period < rdylistptr -> Period)
  {    
    CPU_SR_ALLOC();
    OS_CRITICAL_ENTER();
    OS_RdyListRemove(rdylistptr);           
    OS_CRITICAL_EXIT_NO_SCHED();
    Tree = Insert_heap(rdylistptr, Tree);
    OS_TaskRdy(MinNode->p_tcb_heap); 
    rdylistptr = MinNode->p_tcb_heap;
    Tree = Delete_heap(MinNode->p_tcb_heap, Tree);
  }
}

void rdylist_delete(void)
{
  heap* MinNode = find_min();
  if(MinNode == NULL)
  {
    rdylistptr = NULL;
  }
  else 
  {
    OS_TaskRdy(MinNode->p_tcb_heap); 
    rdylistptr = MinNode->p_tcb_heap;
    OSTaskQty++;
    Tree = Delete_heap(MinNode->p_tcb_heap, Tree);
  }
}



heap* Search_heap(OS_TICK key, heap* Tree)
{
  return Heap(key, Tree);
}

heap* find_min()
{
  heap* current = Tree;
  if(current == NULL)
  {
    return NULL;
  }
  else while(current->lchild != NULL)
  {
    current = current->lchild;
  }
  return current;
}